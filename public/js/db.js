let objectDb = "obj_match";
let dbPromised = idb.open("match_schedule", 1, function(upgradeDb){
    let matchObjectStore = upgradeDb.createObjectStore(objectDb, {
        keyPath: "id"
    });
    matchObjectStore.createIndex("id", "id", {unique: false});
});

function saveForLater(match) {
    dbPromised
    .then(function(db) {
        let tx = db.transaction(objectDb, "readwrite");
        let store = tx.objectStore(objectDb);
        // console.log(match.match);
        store.put(match.match);
        return tx.complete;
    })
    .then(function(){
        M.toast({html: 'Data Match Berhasil Disimpan.&nbsp;&nbsp;<i class="material-icons">check</i>'})
        console.log("Jadwal Pertandingan Berhasil Disimpan");
    })
}


function getAll() {
    return new Promise(function(resolve, reject) {
      dbPromised
        .then(function(db) {
            let tx = db.transaction(objectDb, "readonly");
            let store = tx.objectStore(objectDb);
          return store.getAll();
        })
        .then(function(match) {
          resolve(match);
        });
    });
  }

  function getById(id) {
    return new Promise(function(resolve, reject) {
      dbPromised
        .then(function(db) {
            let tx = db.transaction(objectDb, "readonly");
            let store = tx.objectStore(objectDb);
          return store.get(id);
        })
        .then(function(match) {
          resolve(match);
        });
    });
  }