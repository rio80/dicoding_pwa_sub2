let base_url = "http://api.football-data.org/v2/matches/";
let auth_token = "b082d8622c084ae59eedb50dd8c8752b";
let header_auth_token = {
  'X-Auth-Token': auth_token,
  };
// Blok kode yang akan di panggil jika fetch berhasil
function status(response) {
  if (response.status !== 200) {
    // Method reject() akan membuat blok catch terpanggil
    return Promise.reject(new Error(response.statusText));
  } else {
    // Mengubah suatu objek menjadi Promise agar bisa "di-then-kan"
    return Promise.resolve(response);
  }
}
// Blok kode untuk memparsing json menjadi array JavaScript
function json(response) {
  return response.json();
}
// Blok kode untuk meng-handle kesalahan di blok catch
function error(error) {
  // Parameter error berasal dari Promise.reject()
  console.log("Error : " + error);
}
// Blok kode untuk melakukan request data json
function getMatch() {

    if ('caches' in window) {
        caches.match(base_url).then(function(response) {
          if (response) {
            response.json().then(function (data) {
              let matchHTML = '';
              matchHTML = setToHtml(null, true);
              data.matches.forEach(function(match) {
                matchHTML += setToHtml(match, false);
              });
              // Sisipkan komponen card ke dalam elemen dengan id #content
              document.getElementById("id_match_list").innerHTML = matchHTML;
              
            })
          }
        })
      }
      fetch(base_url, {
        headers: {
          'X-Auth-Token': auth_token,
        }
      })
        .then(status)
        .then(json)
        .then(function(data) {
          // Isi disembunyikan agar lebih ringkas
      })
}

function getMatchById() {
  return new Promise(function(resolve, reject) {
    // Ambil nilai query parameter (?id=)
    var urlParams = new URLSearchParams(window.location.search);
    var idParam = urlParams.get("id");

  //  Untuk Mendapatkan Image Teams
    if ("caches" in window) {
      let imageHomeTeam = null;
      let imageAwayTeam = null;
      let getImage = null;
      caches.match(base_url +  idParam).then(function(response) {
        if (response) {
          response.json().then(function(data) {
            

            fetch("https://api.football-data.org/v2/teams/" + data.match.homeTeam.id, {
              headers: header_auth_token
            })
            .then(status)
            .then(json)
            .then(function(team) {
            
              imageHomeTeam = team.crestUrl;
              getImage += 1;
              // ... kode lain disembunyikan agar lebih ringkas 
              if(getImage == 2){
                setToHtmlDetail(data, imageAwayTeam, imageHomeTeam, true);
              }
              
              // Kirim objek data hasil parsing json agar bisa disimpan ke indexed db
              resolve(team);
            });


            fetch("https://api.football-data.org/v2/teams/" + data.match.awayTeam.id, {
              headers: header_auth_token
            })
            .then(status)
            .then(json)
            .then(function(team) {
            
              imageAwayTeam = team.crestUrl;
              getImage += 1;
              // ... kode lain disembunyikan agar lebih ringkas 
              if(getImage == 2){
                setToHtmlDetail(data, imageAwayTeam, imageHomeTeam, true);
              }
              // Kirim objek data hasil parsing json agar bisa disimpan ke indexed db
              resolve(team);
            });
            
           
            
            
            // Kirim objek data hasil parsing json agar bisa disimpan ke indexed db
            resolve(data);
          });
        }
      });
    }


    fetch(base_url + idParam, {
      headers: header_auth_token
    })
      .then(status)
      .then(json)
      .then(function(data) {
       
        let imageHomeTeam = null;
        let imageAwayTeam = null;
        let getImage = null;
        fetch("https://api.football-data.org/v2/teams/" + data.match.homeTeam.id, {
          headers: {
          'X-Auth-Token': auth_token,
          }
        })
        .then(status)
        .then(json)
        .then(function(team) {
        
          imageHomeTeam = team.crestUrl;
          // ... kode lain disembunyikan agar lebih ringkas 
          if(getImage == 2){
            setToHtmlDetail(data, imageAwayTeam, imageHomeTeam, true);
          }
          // Kirim objek data hasil parsing json agar bisa disimpan ke indexed db
          resolve(team);
        });


        fetch("https://api.football-data.org/v2/teams/" + data.match.awayTeam.id, {
          headers: header_auth_token
        })
        .then(status)
        .then(json)
        .then(function(team) {
        
          imageAwayTeam = team.crestUrl;
          // ... kode lain disembunyikan agar lebih ringkas 
          if(getImage == 2){
            setToHtmlDetail(data, imageAwayTeam, imageHomeTeam, true);
          }
          // Kirim objek data hasil parsing json agar bisa disimpan ke indexed db
          resolve(team);
        });

        
        // ... kode lain disembunyikan agar lebih ringkas 
       
        // Kirim objek data hasil parsing json agar bisa disimpan ke indexed db
        resolve(data);
      });
  });
}


function getSavedMatch() {
  getAll().then(function(match) {
    // Menyusun komponen card artikel secara dinamis
   
    let matchHTML = '';
    matchHTML = setToHtml(null, true);
    match.forEach(function(match) {
      matchHTML += setToHtml(match, false);
    });
    // Sisipkan komponen card ke dalam elemen dengan id #body-content
    document.getElementById("saved").innerHTML = matchHTML;
  });
}


function getSavedMatchById() {
  var urlParams = new URLSearchParams(window.location.search);
  var idParam = urlParams.get("id");
  
  getById(idParam).then(function(match) {
    console.log(match);
    fetch("https://api.football-data.org/v2/teams/" + match.homeTeam.id, {
      headers: header_auth_token
      })
        .then(status)
        .then(json)
        .then(function(team) {
        
          imageHomeTeam = team.crestUrl;
          getImage += 1;
          // ... kode lain disembunyikan agar lebih ringkas 
          if(getImage == 2){
            setToHtmlDetail(match, imageAwayTeam, imageHomeTeam, true);
          }
          
          // Kirim objek data hasil parsing json agar bisa disimpan ke indexed db
          resolve(team);
        });


        fetch("https://api.football-data.org/v2/teams/" + match.awayTeam.id, {
          headers: header_auth_token
        })
        .then(status)
        .then(json)
        .then(function(team) {
        
          imageAwayTeam = team.crestUrl;
          getImage += 1;
          // ... kode lain disembunyikan agar lebih ringkas 
          if(getImage == 2){
            setToHtmlDetail(match, imageAwayTeam, imageHomeTeam, true);
          }
          // Kirim objek data hasil parsing json agar bisa disimpan ke indexed db
          resolve(team);
        });
    // Sisipkan komponen card ke dalam elemen dengan id #content
    document.getElementById("body-content").innerHTML = matchHTML;
  });
}

function setToHtml(obj, setNav){
  
  
  let matchHTML = '';
  

  if(setNav){
    matchHTML = `
             <nav class='teal lighten-1'>
              <div class="nav-wrapper">
                <div class="col s12">
                <a href="#" class="breadcrumb">Match List</a>
                </div>
              </div>
            </nav>`;
           
  }else{

    let hasWinner = obj.score.winner;

    if (hasWinner !== null) {
      hasWinner = `Winner is : ${hasWinner}`;
    }else{
      hasWinner = '';
    }

    let img_place = "<div class='img-place'>No Image</div>";
    if(obj.competition.area.ensignUrl !== null){
      img_place = `<img src="${obj.competition.area.ensignUrl}" class="img-place" width="200" height="140">`;
    }

    let timeMatch = obj.utcDate;
    timeMatch = timeMatch.substring(0, 10);
    matchHTML += `
                
                <div class="card">
                  <div class="card-content area">
                      <span>Place : ${obj.competition.area.name}</span>
                  </div>
                  <div class="card-content">
                      <div class="col m4">
                          ${img_place}
                      </div>
                      <div class="col m8">

                          <div class="card card-versus">
                              <div class="card-content title-league">
                                  <p>${obj.competition.name}</p>
                              </div>
                              <div class="card-content versus versus-title">
                                  Team Player
                              </div>
                              <div class="card-content col m12 versus">
                                  <div class="col m5 s12 text-right">
                                      ${obj.homeTeam.name}
                                  </div>
                                  <div class="col m2 s12">VS</div>
                                  <div class="col m5 s12 text-left">
                                      ${obj.awayTeam.name}
                                  </div>
                              </div>
                              <div class="card-content">
                                  <p>&nbsp;</p>
                              </div>
                              <div class="card-action">
                                  Date : ${timeMatch}<br>
                                  ${hasWinner}
                              </div>
                          </div>

                      </div>
                  </div>
                  <div class="card-content">
                      <div class="m12 link-detail">
                          <a href="./match_detail.html?id=${obj.id}" class="btn-detail">Detail</a>
                      </div>
                  </div>
                </div>
                    `;
  }
  return matchHTML;
}


function setToHtmlDetail(obj, image1, image2, isHasNav){
  let matchHTML = '';
  let tableHTML = '';
  let strHTML = '';
  tableHTML += `<table class="highlight">
            <thead>
              <tr>
                <th>#</th>
                <th>Away Team</th>
                <th>Home Team</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Name</td>
                <td><img src="${image1}" width="50" height="70">
                ${obj.head2head.homeTeam.name}</td>
                <td><img src="${image2}" width="50" height="70">
                ${obj.head2head.awayTeam.name}</td>
              </tr>
              <tr>
                <td>Wins</td>
                <td>${obj.head2head.homeTeam.wins}</td>
                <td>${obj.head2head.awayTeam.wins}</td>
              </tr>
              <tr>
                <td>Draw</td>
                <td>${obj.head2head.homeTeam.draws}</td>
                <td>${obj.head2head.awayTeam.draws}</td>
              </tr>
              <tr>
                <td>Losses</td>
                <td>${obj.head2head.homeTeam.losses}</td>
                <td>${obj.head2head.awayTeam.losses}</td>
              </tr>
              <tr>
              <td>Total Goals</td>
              <td colspan="2">${obj.head2head.totalGoals}</td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
      `;

      matchHTML = `
      <div class="row">
        <div class="col s12 m12">
          <nav class='teal lighten-1'>
            <div class="nav-wrapper">
              <div class="col s12">
                <a href="javascript:window.history.back();" class="breadcrumb">Match List</a>
                <a href="#" class="breadcrumb">Match Detail</a>
              </div>
            </div>
          </nav>
          ${tableHTML}
        </div>
      </div>
      `;

      if(isHasNav){
        strHTML = matchHTML;
      }else{
        strHTML = tableHTML;
      }
      
        document.getElementById("body-content").innerHTML = matchHTML;
      }

 