const CACHE_NAME = 'match_schedule_v2';
const public = '/public';
var urlsToCache = [
  `${public}/`,
  `${public}/index.html`,
  `${public}/assets/images/icon.png`,
  `${public}/pages/nav.html`,
  `${public}/pages/home.html`,
  `${public}/pages/saved.html`,
  `${public}/pages/match_list.html`,
  `${public}/match_detail.html`,
  `${public}/css/materialize.min.css`,
  `${public}/css/materialize.css`,
  `${public}/css/custom.css`,
  `${public}/js/materialize.min.js`,
  `${public}/js/materialize.js`,
  `${public}/js/nav.js`,
  `${public}/js/api.js`,
  `${public}/js/db.js`,
  `${public}/js/idb.js`,
  `${public}/push.js`,
  `/../manifest.json`
];
 
self.addEventListener("install", function(event) {
  event.waitUntil(
    caches.open(CACHE_NAME).then(function(cache) {
      return cache.addAll(urlsToCache);
    })
  );
});

self.addEventListener("fetch", function(event) {
    var base_url = "http://api.football-data.org/v2/matches/";
    if (event.request.url.indexOf(base_url) > -1) {
      event.respondWith(
        caches.open(CACHE_NAME).then(function(cache) {
          return fetch(event.request,
            {
              headers:{
                'X-Auth-Token': 'b082d8622c084ae59eedb50dd8c8752b',
              }
        }).then(function(response) {
            cache.put(event.request.url, response.clone());
            return response;
          })
        })
      );
    } else {
      event.respondWith(
        caches.match(event.request, { ignoreSearch: true }).then(function(response) {
            return response || fetch (event.request);
        })
      )
    }
  });


  self.addEventListener("activate", function(event) {
    event.waitUntil(
      caches.keys().then(function(cacheNames) {
        return Promise.all(
          cacheNames.map(function(cacheName) {
            if (cacheName != CACHE_NAME) {
              console.log("ServiceWorker: cache " + cacheName + " dihapus");
              return caches.delete(cacheName);
            }
          })
        );
      })
    );
  });


  self.addEventListener('notificationclick', function (event) {
    event.notification.close();
    
    if (!event.action) {
      // Penguna menyentuh area notifikasi diluar action
      console.log('Notification Click.');
      return;
    }
    switch (event.action) {
      case 'yes-action':
        console.log('Pengguna memilih action yes.');
        break;
      case 'no-action':
        console.log('Pengguna memilih action no');
        break;
      default:
        console.log(`Action yang dipilih tidak dikenal: '${event.action}'`);
        break;
    }

  });

  self.addEventListener('push', function(event) {
    var body;
    if (event.data) {
      body = event.data.text();
    } else {
      body = 'Push message no payload';
    }
    var options = {
      body: body,
      icon: 'assets/images/icon.png',
      vibrate: [100, 50, 100],
      data: {
        dateOfArrival: Date.now(),
        primaryKey: 1
      },
      actions: [{action: "yes-action", title: "Buka Link"}]
    };
    event.waitUntil(
      self.registration.showNotification('Push Notification', options)
    );
  });

